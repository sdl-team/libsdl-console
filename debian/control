Source: libsdl-console
Section: libs
Priority: optional
Maintainer: Debian SDL packages maintainers <pkg-sdl-maintainers@lists.alioth.debian.org>
Uploaders: Manuel A. Fernandez Montecelo <mafm@debian.org>
Standards-Version: 4.2.1
Rules-Requires-Root: no
Build-Depends: debhelper (>= 11~),
               libsdl1.2-dev (>= 1.2.14~),
               libsdl-image1.2-dev (>= 1.2.12~)
Homepage: https://sourceforge.net/projects/sdlconsole/
Vcs-Browser: https://salsa.debian.org/sdl-team/libsdl-console
Vcs-Git: https://salsa.debian.org/sdl-team/libsdl-console.git


Package: libsdl-console
Architecture: any
Multi-Arch: same
Pre-Depends: ${misc:Pre-Depends}
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: Console that can be added to any SDL application, libraries
 This library provides a console similar to the consoles in Quake and other
 games but with lots of added features.
 .
 A console is meant to be a very simple way of interacting with a program
 and executing commands. Commands are linked to the console with callback
 functions so that when a command is typed in, a specific function is
 executed automatically.
 .
 This package contains the runtime library.

Package: libsdl-console-dev
Section: libdevel
Architecture: any
Depends: ${misc:Depends},
         libsdl-console (= ${binary:Version}),
         libsdl1.2-dev (>= 1.2.14~),
         libsdl-image1.2-dev (>= 1.2.12~)
Description: Console that can be added to any SDL application, development files
 This library provides a console similar to the consoles in Quake and other
 games but with lots of added features.
 .
 A console is meant to be a very simple way of interacting with a program
 and executing commands. Commands are linked to the console with callback
 functions so that when a command is typed in, a specific function is
 executed automatically.
 .
 This package contains the header files and static library needed to
 compile applications that use libsdl-console.
